#Jordan Starr
#Began code: 10/11/12
#Modified code: 10/12/2012
#Finished code: 10/12/2012

import urllib,re,win32com,datetime
import win32com.client as win32 
import sys



#PURPOSE --> To parse geographic and business data from Canada 411 to Excel, where it can then be geocoded into 
#ArcGIS or other GIS software. For test purposes we I am using LCBO records from Canada 411

#**************************************************************************************************************
#User-Defined Function For Parsing 

def parse(s,leader, trailer): # function to parse information between a leading string and a trailing string
        try:
            end_of_leader = s.index(leader) + len(leader)
            start_of_trailer = s.index(trailer, end_of_leader)
            return s[end_of_leader:start_of_trailer]
        except:
            return ""

#***************************************************************************************************************
#Taking Excel functions out of loop so that each page of records goes in the same sheet of excel 
#Object is LCBO URL
#"s" will read the file
#Results are stored in "resultlist" dictionary
#Creating pageNum variable and changing from str to int for addition in the URL

excelobj = win32com.client.Dispatch("Excel.Application")  #(1.)
excelobj.Workbooks.Add()                                  #(1.)               
pageNum = 1
row = 2
while pageNum<7:
        pageNum = (str(pageNum))
        fileobj = urllib.urlopen("http://canada411.yellowpages.ca/search/si/" + pageNum + "/lcbo/oakville?showDD=true")
        s = fileobj.read()             
        resultlist = list()             
        y = 0
        y= y+1
        pageNum = (int(pageNum))
        pageNum += 1 
  
    
#***************************************************************************************************************
#Spliting each listing by value: "splititems" in source code
#Parsing out: Business name, Address, Phone Number
#Based off parsed 'Address' from URL - I am parsing out City, Address, and Postal Code by refrencing seperators in the text outputted
#Storing all results in dictionary: "result list"
#Appended dictionary further to seperate address, postal code, and city

        splititems = s.split("YP_SHARE.generateBarResult")      
        for listing in splititems:
            try:
                title = parse(listing,"<span class=\"listingTitle\">","</span>")    
                address = parse(listing,"<div class=\"address\">","</div>")         
                phone = parse(listing,"<div class=\"phoneNumber\">","</div>")       
                postalcode = parse(listing,"<div class=\"address\">","</div>") 
                City = parse(address,",",",")
                address1 = parse(address,"",",")
                PostalCode = address[-7:]
                y = 6                                                           
            except: 
                pass   
            resultlist.append({'businessname':title,'address':address1,'phone':phone, 'PostalCode':PostalCode,'City':City }) # Populate the result in list containing a dictionary            
        fileobj.close()   # close the file after reading all of the contents


#***************************************************************************************************************
#Making Excel sheet 1 visible
#Input Headers across row 1 in excel to include Business Name, Address, Phone, Postal Code, City
#Bold each of the headings
        
        excelobj.Visible = 1                                   
        sh = excelobj.ActiveSheet              
        sh.Cells(1,1).Value = 'Business Name'   
        sh.Cells(1,1).Font.Bold = True
        sh.Cells(1,2).Value = 'Address'
        sh.Cells(1,2).Font.Bold = True
        sh.Cells(1,3).Value = 'Phone'
        sh.Cells(1,3).Font.Bold = True
        sh.Cells(1,4).Value = 'PostalCode'        
        sh.Cells(1,4).Font.Bold = True
        sh.Cells(1,5).Value = 'City'
        sh.Cells(1,5).Font.Bold = True

#***************************************************************************************************************
#For statement takes information from dictionary and assigns it to rows 1-5
#row +=1 - this statement switches to next row using increment.
#Print function shows how many rows are being written into excel - this command is seen in the python Shell        
        for business in resultlist: 
            sh.Cells(row,1).Value = business["businessname"]  
            sh.Cells(row,2).Value = business["address"]      
            sh.Cells(row,3).Value = business["phone"]
            sh.Cells(row,4).Value = business["PostalCode"]
            sh.Cells(row,5).Value = business["City"]
            row +=1                             
        print str(len(resultlist)) + ' rows written to excel.'
        
# END CODE 
