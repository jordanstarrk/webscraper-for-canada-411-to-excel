#Python Webscraping Script for Canada 411 to Excel
Purpose is to parse geographic and business data from Canada 411 to Excel, where it can then be geocoded into ArcGIS or other GIS software. 
For test purposes we I am using LCBO records from Canada 411. This will scrape the following attributes: (business name, address, phone, postal code, city)

Created by: Jordan Starr
Began code: 10/11/12
Modified code: 10/12/2012
Finished code: 10/12/2012

###Prerequisites
- Python version 2.7
- Modules required: urllib,re, win32com, datetime, sys
